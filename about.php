<?php include_once('header.php'); ?>

<!-- INNER PAGE TOP -->
<div id="jsdInnerPageTop" class="jsd-breadcrumb-section">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-sm-12">
				<nav class="breadcrumb">
					<a class="breadcrumb-item" href="index.php"><i class="fa fa-home"></i></a>
					<span class="breadcrumb-item active">About</span>
				</nav>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>

<!-- INNER PAGES CONTAINER -->
<section class="jsd-section jsd-main-content">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-lg-9 push-lg-3 jsd-main">
				<h1>About Us</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis, eaque maxime, corporis quibusdam eos nobis inventore, sit eius animi, porro placeat ea praesentium illo amet reiciendis. Non, fugiat, illum.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, animi sit iusto odio aperiam nam. Repellat, consequatur, soluta tempore eligendi earum odio explicabo reiciendis ratione ea fugiat mollitia unde perspiciatis.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse molestias fugit, corrupti possimus aliquid? Obcaecati facere, possimus eaque nemo. Reiciendis libero dignissimos est repellendus neque cumque, eligendi, delectus et natus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam eius deserunt ad rerum impedit dignissimos ipsa blanditiis harum excepturi, omnis ducimus ipsum architecto quae consequuntur iure inventore facere, tenetur voluptates.</p>

				<img src="assets/build/img/plan.jpg" alt="" class="img-fluid">

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio nisi nobis numquam dolorem accusantium dolore explicabo reprehenderit in? Quisquam consequuntur sunt, fuga dicta rem cumque sit ex, atque soluta vitae.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et animi ex, incidunt inventore perspiciatis. Tempora, incidunt, placeat veniam quis sint officia error, eius dolor dicta omnis possimus reiciendis molestias vero.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum reiciendis cum illo et omnis voluptatem consequatur earum eos doloribus deserunt facilis harum, minus sunt commodi blanditiis ab rerum modi dolores.</p>
			</div>

			<div class="col-lg-3 pull-lg-9 jsd-sidebar">
				<div class="jsd-sidebar-menu">
					<ul class="jsd-sb-menu-list">
						<li class="active"><a href="">About JSD Engineers</a></li>
						<li><a href="careers.php">Careers</a></li>
						<li><a href="contact.php">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="clearfix"></div>
<?php include_once('footer.php'); ?>