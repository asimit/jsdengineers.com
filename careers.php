<?php include_once('header.php'); ?>

<!-- INNER PAGE TOP -->
<div id="jsdInnerPageTop" class="jsd-breadcrumb-section">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-sm-12">
				<nav class="breadcrumb">
					<a class="breadcrumb-item" href="index.php"><i class="fa fa-home"></i></a>
					<span class="breadcrumb-item active">Careers</span>
				</nav>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>

<!-- INNER PAGES CONTAINER -->
<section class="jsd-section jsd-main-content">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-lg-9 push-lg-3 jsd-main">
				<h1>Careers</h1>
				<!-- Card FOR No Opening Message -->
				<div class="card card-vacancy">
					<div class="card-block">
						<p class="text-muted"><i class="fa fa-exclamation-triangle"></i> There is No Opening at Present. Keep in Touch with us.</p>
					</div>
				</div>

				<!-- Card Style for Openings -->
				<div class="card card-vacancy">
					<div class="card-block">
						<h4 class="card-title">Civil Engineer Level 4 Vacancy</h4>
						<hr>
						<p>We are in search of Civil Engineers to join our company as soon as possible. We would like to see Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus asperiores temporibus animi qui, reiciendis aut consequatur, libero! Odit voluptatum vitae cum quibusdam, vel voluptate voluptatem quis excepturi. Officiis, harum, modi.</p>
						<h5><strong>Requirements:</strong></h5>

						<ul class="jsd-custom-list">
							<li>At Least Bachelors of Engineering from Reputed University</li>
							<li>At least 3 Years of Professional Experience</li>
							<li>Age not Exceeding 35 Years</li>
							<li>Able to design drafts and structures of complex buildings, bridges, stadium, parks, millitary base</li>
						</ul>
						<button type="button" class="btn btn-sm btn-outline-warning">Send Resume at: email@myemail.com</button>
					</div>
				</div>
			</div>

			<div class="col-lg-3 pull-lg-9 jsd-sidebar">
				<div class="jsd-sidebar-menu">
					<ul class="jsd-sb-menu-list">
						<li><a href="">About JSD Engineers</a></li>
						<li class="active"><a href="careers.php">Careers</a></li>
						<li><a href="contact.php">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="clearfix"></div>
<?php include_once('footer.php'); ?>