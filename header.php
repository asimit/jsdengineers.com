<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>JSD Engineers Website</title>
        <meta name="description" content="JSD Engineers Website">
        <meta name="author" content="globalitechsystems">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="assets/build/img/favicon.png">
        <link rel="stylesheet" href="style.css" type="text/css">
        <link rel="stylesheet" href="assets/build/css/main.css" type="text/css">
    </head>
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <body>

    <div id="page">
        <!-- MAIN HEADER -->
        <header id="masthead" class="site-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div id="siteLogo">
                            <a href="index.php" title="JSDEngineers"><img src="./assets/build/img/logo.png" class="img-fluid" alt="JSD"></a>
                        </div>
                    </div>

                    <div class="toggleButton">
                        <button class="menu-toggle" id="toggleMenu"><span></span><span></span><span></span><span></span></button>
                    </div>

                    <div class="col-lg-9 col-md-6 right-navs">
                        <div class="top-accessories">
                            <a href="javascript: void(0);" class="jsd-btn jsd-btn-sm" id="projectRequestBtn" data-toggle="modal" data-target="#proposalForm">Submit Your Project Request</a>
                            <a href="tel:‎8328669488" class="jsd-call">‎832-866-9488</a>
                        </div>

                        <div class="clearfix"></div>

                        <nav id="primaryNav" class="main-navigation">
                            <ul class="site-navigation">
                                <li class="menu-item menu-is-active"><a href="index.php">Home</a></li>
                                <li class="menu-item menu-has-children">
                                    <a href="javascript: void(0);">About</a>

                                    <ul class="sub-menu">
                                        <li class="sub-menu-item"><a href="about.php">About JSD Engineers</a></li>
                                        <li class="sub-menu-item"><a href="careers.php">Careers</a></li>
                                        <li class="sub-menu-item"><a href="contact.php">Contact Us</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-has-children">
                                    <a href="javascript: void(0):">Services</a>

                                    <ul class="sub-menu">
                                        <li class="sub-menu-item"><a href="services.php">Residential Home Building</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Commercial Home Building</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Condominions</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Bridge Construction</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Stadium Construction</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Hotels Construction</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Medical Buildings</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-has-children">
                                    <a href="javascript: void(0):">Projects</a>

                                    <ul class="sub-menu">
                                        <li class="sub-menu-item"><a href="services.php">Residential Home Building</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Commercial Home Building</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Condominions</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Bridge Construction</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Stadium Construction</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Hotels Construction</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Medical Buildings</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-has-children">
                                    <a href="javascript: void(0):">Expertise</a>

                                    <ul class="sub-menu">
                                        <li class="sub-menu-item"><a href="services.php">Structural Build</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Design Build</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Construction</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Design Modeling</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-has-children">
                                    <a href="javascript: void(0):">Industries</a>

                                    <ul class="sub-menu">
                                        <li class="sub-menu-item"><a href="services.php">Hotel</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Millitary</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Educational Buildings</a></li>
                                        <li class="sub-menu-item"><a href="services.php">Corporate Building</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a href="">Resources</a></li>
                            </ul>
                        </nav>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </header>
