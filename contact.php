<?php include_once('header.php'); ?>

<!-- INNER PAGE TOP -->
<div id="jsdInnerPageTop" class="jsd-breadcrumb-section">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-sm-12">
				<nav class="breadcrumb">
					<a class="breadcrumb-item" href="index.php"><i class="fa fa-home"></i></a>
					<span class="breadcrumb-item active">Contact</span>
				</nav>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>

<!-- INNER PAGES CONTAINER -->
<section class="jsd-section jsd-main-content">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-lg-9 push-lg-3 jsd-main">
				<h1>Contact Us</h1>
				<div class="row">
					<div class="col-lg-6">
						<address>
							JSD Engineers LLC <br>
							2511 Orchid Creek Dr.<br>
							Pearland, TX 77584<br>
							USA<br>
                            <a href="mailto:info@jsdengineers.com">info@jsdengineers.com</a>
						</address>

						<hr>

						<form class="row contact-form">
						    <div class="col-lg-12">
						        <div class="form-group">
						            <label for="contactName">Full Name</label>
						            <input type="text" class="form-control" id="contactName" placeholder="Full Name">
						        </div>
						    </div>
						    <div class="col-lg-12">
						        <div class="form-group">
						            <label for="ContactEmail">Email Address</label>
						            <input type="email" class="form-control" id="ContactEmail" placeholder="Email Address">
						        </div>
						    </div>
						    <div class="col-lg-12">
						        <div class="form-group">
						            <label for="contactMessage">Your Message</label>
						            <textarea name="message" id="contactMessage" rows="5" class="form-control" placeholder="Your Message"></textarea>
						        </div>
						    </div>
						    <div class="col-lg-12">
						        <div class="form-group">
						            <input type="submit" class="btn btn-primary" value="SUBMIT &rarr;">
						        </div>
						    </div>
						</form>
					</div>

					<div class="col-lg-6 contact-map">
						<div id="jsdContactMap"></div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 pull-lg-9 jsd-sidebar">
				<div class="jsd-sidebar-menu">
					<ul class="jsd-sb-menu-list">
						<li><a href="">About JSD Engineers</a></li>
						<li><a href="careers.php">Careers</a></li>
						<li class="active"><a href="contact.php">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="clearfix"></div>
<?php include_once('footer.php'); ?>