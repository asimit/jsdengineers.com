<?php include_once('header.php'); ?>

<!-- HERO CAROUSEL -->
<div id="jsdHeroCarousel" class="jsd-hero-carousel slider-pro">
	<ul id="jsdSlide">
		<!-- Slide 1 -->
		<li data-img="assets/build/img/banner0.jpg">
			<img class="sp-image sr-only" src="assets/build/img/banner0.jpg" alt="Slider One" />
			<div class="jsdslide-caption">
				<h3>Welcome To Our Site</h3>
			</div>
		</li>
		
		<!-- Slide 2 -->
		<li data-img="assets/build/img/banner1.jpg">
			<img class="sp-image sr-only" src="assets/build/img/banner1.jpg" alt="Slider One" />
			<div class="jsdslide-caption">
				<h3>We ar Awesome </h3>
			</div>
		</li>
	</ul>

	<button id="jsdMoveDown" class="move-down-arrow" data-target="#twoPartFull">
		<span class="jsd-arrow-down"></span>
	</button>
</div>

<div class="clearfix"></div>

<!-- ABOUT US SECTION -->
<section id="twoPartFull" class="jsd-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="jsd-content">
					<h1>Welcome to JSD Engineers</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde rerum molestiae commodi itaque exercitationem similique odio, maxime quod consequuntur, vel cum at voluptates officia nobis a quas aliquid magnam vero!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, labore perspiciatis aut est! Et maxime omnis quos, delectus ab autem esse magnam, sed nobis veritatis eligendi nulla, cumque iusto rerum.</p>
					<a href="about.php" class="jsd-btn jsd-btn-lg">Read More</a>
				</div>
			</div>
		</div>
	</div>
	<div class="jsd-image" data-img="assets/build/img/plan.jpg"></div>
</section>

<div class="clearfix"></div>

<!-- SERVICES LIST SECTION -->
<section id="jsdServices" class="jsd-list-card jsd-section">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h1>Our Services</h1>
			</div>

			<div class="clearfix"></div>

			<?php for ($i=0; $i < 9; $i++) { ?>
			<div class="col-lg-4 col-md-6">
				<div class="jsd-card">
					<a href="">
						<figure class="card-image" data-img="assets/build/img/service<?= $i; ?>.jpg">
							<figcaption>
								<h2>Service Name</h2>
							</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<div class="clearfix"></div>

<!-- OUR PROJECTS SECTION -->
<section id="jsdProjects" class="jsd-list-card jsd-section" data-img="assets/build/img/banner0.jpg">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h1>Our Projects</h1>
			</div>

			<div class="clearfix"></div>

			<?php for ($i=0; $i < 5; $i++) { ?>
			<div class="col-md-6
				<?php if ($i < 2) {
					echo "col-lg-6";
				} else{
					echo "col-lg-4";
				}
				?>
			">
				<div class="jsd-card">
					<a href="">
						<figure class="card-image" data-img="assets/build/img/service<?= $i; ?>.jpg">
							<figcaption>
								<h2>Project Category</h2>
							</figcaption>
						</figure>
					</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>

<div class="clearfix"></div>

<!-- CUSTOMER TESTIMONIALS -->
<section class="jsd-section jsd-testimonials" id="jsdTestimonials" data-title="Our Happy Customers">
    <div class="container jsd-content">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="sr-only">Our Happy Customers</h1>
            </div>
            <div class="col-sm-8 offset-sm-4 testimonials-holder">
                <ul id="jsdCustomerSaying">
                    <li>
                        <p class="testimonial-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus ipsum ratione consequuntur dolores consectetur voluptate in dicta suscipit dolorem ex illo ad asperiores, quidem quo maxime doloribus expedita, excepturi.
                        </p>
                        <p class="testimonial-person">
                            Johny Alberta
                        </p>
                        <p class="testimonial-company">Robert D'costa</p>
                    </li>
                    <li>
                        <p class="testimonial-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae numquam recusandae modi aperiam cumque nobis atque, fuga perferendis aut quibusdam animi incidunt quia, laborum doloremque consectetur obcaecati voluptatibus
                        </p>
                        <p class="testimonial-person">Johnson Alberta</p>
                        <p class="testimonial-company">VolksWagen &amp; Co.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>
<?php include_once('footer.php'); ?>