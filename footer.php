            <!-- OUR ASSOSIATIONS -->
            <section id="jsdAssociationList" class="jsd-section">
                <div class="container">
                    <div class="col-sm-12">
                        <ul id="jsdCertificateList">
                            <?php for ($i=1; $i < 7; $i++) { ?>
                            <li>
                                <img class="sp-image img-fluid" src="assets/build/img/<?= $i; ?>.png" alt="associate" />
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </section>

            <div class="clearfix"></div>
            
            <!-- FOOTER SECTION -->
			<footer id="mastFoot" class="site-footer">
				<div class="container">
					<div class="footer-info-content">
						<div class="row">
							<div class="col-lg-4">
								<div class="footer-widget footer-widget-list">
									<h1>Our Services</h1>
									<ul class="jsd-custom-list">
										<li><a href="">Residential Building</a></li>
										<li><a href="">Commercial Building</a></li>
										<li><a href="">Condominions</a></li>
										<li><a href="">Bridge and Stadium</a></li>
										<li><a href="">Industries</a></li>
									</ul>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="footer-widget footer-widget-text">
									<h1>Contact Information</h1>
									<address>
										JSD Engineers LLC <br>
										2511 Orchid Creek Dr.<br>
										Pearland, TX 77584<br>
										USA<br>
                                        <a href="mailto:info@jsdengineers.com">info@jsdengineers.com</a>
									</address>
								</div>
							</div>

							<div class="col-lg-4">
								<div class="footer-widget footer-widget-map">
									<h1>Our Location</h1>
									<div id="jsdMap"></div>
								</div>
							</div>
						</div>

						<div class="footer-socialize">
							<div class="row">
								<div class="col-md-6 col-lg-8">
									<ul class="social-links">
										<li><a href=""><i class="fa fa-facebook"></i></a></li>
										<li><a href=""><i class="fa fa-twitter"></i></a></li>
										<li><a href=""><i class="fa fa-youtube"></i></a></li>
									</ul>
								</div>
								<div class="col-md-6 col-lg-4">
									<div class="jsdsubscribe">
										<div class="input-group">
											<input type="text" class="form-control" placeholder="Email Address">
											<span class="input-group-btn">
												<button class="btn btn-secondary" type="button">Subscribe</button>
											</span>
									    </div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Copyright Info -->
					<div class="copyright-footer">
						<div class="row">
							<div class="col-12">
								&copy; Copyright <?php echo date('Y'); ?>. All Rights Reserved. <a href="site-policy.php">Privacy Policy</a> <span class="pull-right">Powered By: <a href="http://globalitechsystems.com" title="GlobaliTechSystems Calgary" target="_blank"> GIS CALGARY</a></span>
							</div>
						</div>
					</div>
				</div>
			</footer>
        </div><!--END of Page-->

        <!-- MODAL FORM -->
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="proposalForm" id="proposalForm" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        Submit Your Project Request

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="row proposal-request-form">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="category">Choose Category</label>
                                    <select class="form-control" id="category">
                                        <option value="CategoryOne">Category One</option>
                                        <option value="CategoryTwo">Category Two</option>
                                        <option value="CategoryThree">Category Three</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="subcategory">Choose Sub-Category</label>
                                    <select class="form-control" id="subcategory">
                                        <option value="CategoryOne">Category One</option>
                                        <option value="CategoryTwo">Category Two</option>
                                        <option value="CategoryThree">Category Three</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="fullName">Full Name</label>
                                    <input type="text" class="form-control" id="fullName" placeholder="Full Name">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="emailAddress">Email Address</label>
                                    <input type="email" class="form-control" id="emailAddress" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input type="text" class="form-control" id="address" placeholder="Address">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="telNumber">Phone Number</label>
                                    <input type="tel" class="form-control" id="telNumber" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="message">Your Message</label>
                                    <textarea name="message" id="message" rows="5" class="form-control" placeholder="Your Message"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="SUBMIT &rarr;">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </body>
    <script src="assets/build/js/jquery.min.js" type="text/javascript"></script>
    <script src="assets/build/js/script.min.js" type="text/javascript"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQaKKKlnzn-Qf7Tw6cynDFwIRWUmWphgM&callback=initMap"></script>
</html>