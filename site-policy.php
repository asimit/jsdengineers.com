<?php include_once('header.php'); ?>

<!-- INNER PAGE TOP -->
<div id="jsdInnerPageTop" class="jsd-breadcrumb-section">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-sm-12">
				<nav class="breadcrumb">
					<a class="breadcrumb-item" href="index.php"><i class="fa fa-home"></i></a>
					<span class="breadcrumb-item active">Privacy Policy</span>
				</nav>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>

<!-- INNER PAGES CONTAINER -->
<section class="jsd-section jsd-main-content">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-lg-12 jsd-main">
				<h1>Privacy Policy</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate debitis, eaque maxime, corporis quibusdam eos nobis inventore, sit eius animi, porro placeat ea praesentium illo amet reiciendis. Non, fugiat, illum.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, animi sit iusto odio aperiam nam. Repellat, consequatur, soluta tempore eligendi earum odio explicabo reiciendis ratione ea fugiat mollitia unde perspiciatis.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse molestias fugit, corrupti possimus aliquid? Obcaecati facere, possimus eaque nemo. Reiciendis libero dignissimos est repellendus neque cumque, eligendi, delectus et natus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam eius deserunt ad rerum impedit dignissimos ipsa blanditiis harum excepturi, omnis ducimus ipsum architecto quae consequuntur iure inventore facere, tenetur voluptates.</p>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio nisi nobis numquam dolorem accusantium dolore explicabo reprehenderit in? Quisquam consequuntur sunt, fuga dicta rem cumque sit ex, atque soluta vitae.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et animi ex, incidunt inventore perspiciatis. Tempora, incidunt, placeat veniam quis sint officia error, eius dolor dicta omnis possimus reiciendis molestias vero.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum reiciendis cum illo et omnis voluptatem consequatur earum eos doloribus deserunt facilis harum, minus sunt commodi blanditiis ab rerum modi dolores.</p>

				<p><strong>The Site Use Policy are:</strong></p>

				<ul class="jsd-custom-list">
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam voluptates eveniet qui sapiente sequi aliquid, veritatis fuga, non nisi ratione consequuntur asperiores, sit aliquam ducimus incidunt assumenda soluta nulla velit.</li>
					<li>Tempora, incidunt, placeat veniam quis sint officia error, eius dolor dicta omnis possimus reiciendis molestias vero.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et voluptate exercitationem, aperiam nobis harum quia sed.</li>
				</ul>
				<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo neque vitae nulla consequatur. Vitae omnis atque sunt a est, dolorum mollitia sint suscipit, corrupti magni corporis ex aspernatur molestiae veritatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, dicta laboriosam autem. Id cupiditate debitis, ratione, illo repellat autem tempore officia enim eos aliquid sit expedita consectetur porro qui placeat!</p>
			</div>
		</div>
	</div>
</section>

<div class="clearfix"></div>
<?php include_once('footer.php'); ?>