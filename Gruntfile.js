module.exports = function( grunt ){
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
		    dist: {
				options: {
					style: 'expanded'
				},
				files: {
					'./assets/src/css/main.css': './assets/src/sass/below-fold/main.scss',
					'./style.css' : './assets/src/sass/above-fold/style.scss'
				}
			}
		},

		concat: {
			options:{
				separator:"\n /*** New File ***/ \n"
			},
			js: { 
				src: [
					'./node_modules/bootstrap/js/dist/util.js',
					'./node_modules/bootstrap/js/dist/modal.js',
					'./node_modules/lightslider/dist/js/lightslider.min.js',
					'./assets/src/js/script.js',
				],
				dest: './assets/build/js/script.js'
			},
			css: {
				src: [
					'./node_modules/font-awesome/css/font-awesome.css',
					'./node_modules/lightslider/dist/css/lightslider.min.css',
					'./assets/src/css/*.css'
				],
				dest: './assets/build/css/main.css'
			}
		},

		uglify: {
			options: {
				report: 'gzip',
				banner: '/*! Script for: <%= pkg.name %> Created On: <%= grunt.template.today("yyyy-mm-dd") %> by: Asim Subedi (asimsubedi.com.np) */\n',
			},
			main: {
				src: ['./assets/build/js/script.js'],
				dest: './assets/build/js/script.min.js'
			}
		},

		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1,
				keepSpecialComments : 0
			},
			target: {
				files: [{
					expand: true,
					cwd: './assets/build/css/',
					src: ['*.css', '!*.min.css'],
					dest: './assets/build/css/',
					ext: '.min.css'
				}]
			}
		},

		copy: {
			img: {
			    files: [
			        {
			            expand: true,
			            cwd: "./assets/src/img",
			            src: "**",
			            dest: "./assets/build/img/",
			            filter: "isFile"
			        },
			        {
			            expand: true,
			            cwd: "./node_modules/lightslider/dist/img",
			            src: "**",
			            dest: "./assets/build/img/",
			            filter: "isFile"
			        }
			    ]
			},
			fonts:{
			    files: [{
			        expand: true,
			        cwd: './node_modules/font-awesome/fonts',
			        src: '**',
			        dest: './assets/build/fonts/',
			        filter: 'isFile'
			    }]
			},
			js:{
			    files: [{
			        expand: true,
			        cwd: './node_modules/jquery/dist/',
			        src: 'jquery.min.js',
			        dest: './assets/build/js/',
			        filter: 'isFile'
			    }]
			}
		},
		
		watch: {
			js : {
				files : [
					'./assets/src/js/*.js'
				],
				tasks : ['js']
				
			},
			css : {
				files : [
					'./assets/src/sass/**/*.scss',
					'./assets/src/sass/above-fold/*.scss',
					'./assets/src/sass/below-fold/*.scss',
					'./assets/src/sass/below-fold/**/*.scss'
				],
				tasks : ['css']
			},
			img: {
                files: [
                    './assets/src/img/*'
                ],
                tasks: ['img']
            },
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	// grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask( 'css', [ 'sass', 'concat:css', 'cssmin'] );
    grunt.registerTask( 'js', [ 'concat:js', 'uglify' ] );
    grunt.registerTask('img', ['copy:img']);

    grunt.registerTask('default', ['sass', 'concat','uglify','cssmin', 'copy'] );
}