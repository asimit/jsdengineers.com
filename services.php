<?php include_once('header.php'); ?>

<!-- INNER PAGE TOP -->
<div id="jsdInnerPageTop" class="jsd-breadcrumb-section">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-sm-12">
				<nav class="breadcrumb">
					<a class="breadcrumb-item" href="index.php"><i class="fa fa-home"></i></a>
					<span class="breadcrumb-item active">Services</span>
				</nav>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>

<!-- INNER PAGES CONTAINER -->
<section class="jsd-section jsd-main-content">
	<div class="container jsd-content">
		<div class="row">
			<div class="col-lg-9 push-lg-3 jsd-main">
				<h1><small>Projects - </small>Condominions</h1>
				<!-- Service One -->
				<div class="card card-service-display" id="serviceCard0">
					<div class="card-block">
						<h4 class="card-title">Phoenix West II</h4>
						<ul class="jsd-service-info">
							<li><i class="fa fa-map-marker"></i> Orange Beach, AL</li>
							<li><i class="fa fa-calendar"></i> 2015</li>
							<li><i class="fa fa-check complete"></i> Completed</li>
						</ul>
						<hr>
						<div class="row">
							<div class="col-md-7 service-describe">
								<p>This 360,000 sf structure consists of 22 occupied floors plus roof, stair and elevator tower slabs, along with a six-story parking garage with a 7th floor for tennis courts with reinforced cast-in-place concrete with post-tension concrete floor slabs. The structure sits on concrete pile caps supported by 435 pre-stressed concrete pilings bearing approximately 66 ft below sea-level. This project is currently in the construction phase and set for completion in 2017.</p>

								<table class="table table-sm">
									<tbody>
										<tr>
											<th>Architect:</th>
											<td>JSD Engineers</td>
										</tr>
										<tr>
											<th>Area:</th>
											<td>15000 sq. Ft.</td>
										</tr>
										<tr>
											<th>Stories:</th>
											<td>5</td>
										</tr>
										<tr>
											<th>Cost:</th>
											<td>$ 125,432,120</td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="col-md-5 service-gallery">
								<ul id="serviceGallerySlide">
									<li><img src="assets/build/img/service0.jpg" class="img-fluid" alt=""></li>
									<li><img src="assets/build/img/service1.jpg" class="img-fluid" alt=""></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!-- Service Two -->
				<div class="card card-service-display" id="serviceCard1">
					<div class="card-block">
						<h4 class="card-title">Palms Of Perdido</h4>
						<ul class="jsd-service-info">
							<li><i class="fa fa-map-marker"></i> San Fransisco Bay, CA</li>
							<li><i class="fa fa-calendar"></i> 2016</li>
							<li><i class="fa fa-times incomplete"></i> Work on Progress</li>
						</ul>
						<hr>
						<div class="row">
							<div class="col-md-7 service-describe">
								<p>This 360,000 sf structure consists of 22 occupied floors plus roof, stair and elevator tower slabs, along with a six-story parking garage with a 7th floor for tennis courts with reinforced cast-in-place concrete with post-tension concrete floor slabs. The structure sits on concrete pile caps supported by 435 pre-stressed concrete pilings bearing approximately 66 ft below sea-level. This project is currently in the construction phase and set for completion in 2017.</p>

								<table class="table table-sm">
									<tbody>
										<tr>
											<th>Architect:</th>
											<td>JSD Engineers</td>
										</tr>
										<tr>
											<th>Area:</th>
											<td>15000 sq. Ft.</td>
										</tr>
										<tr>
											<th>Stories:</th>
											<td>5</td>
										</tr>
										<tr>
											<th>Cost:</th>
											<td>$ 125,432,120</td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="col-md-5 service-gallery">
								<ul id="serviceGallerySlide">
									<li><img src="assets/build/img/service0.jpg" class="img-fluid" alt=""></li>
									<li><img src="assets/build/img/service1.jpg" class="img-fluid" alt=""></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!-- Service Three -->
				<div class="card card-service-display" id="serviceCard2">
					<div class="card-block">
						<h4 class="card-title">Royal Pomodro Casion Club</h4>
						<ul class="jsd-service-info">
							<li><i class="fa fa-map-marker"></i> Texas Street, TX</li>
							<li><i class="fa fa-calendar"></i> 2016</li>
							<li><i class="fa fa-check completed"></i> Completed</li>
						</ul>
						<hr>
						<div class="row">
							<div class="col-md-7 service-describe">
								<p>This 360,000 sf structure consists of 22 occupied floors plus roof, stair and elevator tower slabs, along with a six-story parking garage with a 7th floor for tennis courts with reinforced cast-in-place concrete with post-tension concrete floor slabs. The structure sits on concrete pile caps supported by 435 pre-stressed concrete pilings bearing approximately 66 ft below sea-level. This project is currently in the construction phase and set for completion in 2017.</p>

								<table class="table table-sm">
									<tbody>
										<tr>
											<th>Architect:</th>
											<td>JSD Engineers</td>
										</tr>
										<tr>
											<th>Area:</th>
											<td>15000 sq. Ft.</td>
										</tr>
										<tr>
											<th>Stories:</th>
											<td>5</td>
										</tr>
										<tr>
											<th>Cost:</th>
											<td>$ 125,432,120</td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="col-md-5 service-gallery">
								<ul id="serviceGallerySlide">
									<li><img src="assets/build/img/service0.jpg" class="img-fluid" alt=""></li>
									<li><img src="assets/build/img/service1.jpg" class="img-fluid" alt=""></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 pull-lg-9 jsd-sidebar">
				<div class="jsd-sidebar-menu">
					<ul class="jsd-sb-menu-list">
						<li><a href="services.php">Residential Home Building</a></li>
						<li><a href="services.php">Commercial Home Building</a></li>
						<li class="active">
							<a href="services.php">Condominions</a>

							<ul class="jsd-sb-sub-projects">
								<li><a href="" data-target="#serviceCard0">Phoenix West II</a></li>
								<li><a href="" data-target="#serviceCard1">Palms Of Perdido</a></li>
								<li><a href="" data-target="#serviceCard2">Royal Pomodro Casion Club</a></li>
							</ul>
						</li>
						<li>
							<a href="services.php">Bridge Construction</a>
							<ul class="jsd-sb-sub-projects">
								<li><a href="" data-target="#serviceCard0">Link to First</a></li>
								<li><a href="" data-target="#serviceCard1">Secondary attach</a></li>
								<li><a href="" data-target="#serviceCard2">Third one is Linked</a></li>
								<li><a href="" data-target="#serviceCard3">Fourth Project</a></li>
							</ul>
						</li>
						<li><a href="services.php">Stadium Construction</a></li>
						<li><a href="services.php">Hotels Construction</a></li>
						<li><a href="services.php">Medical Buildings</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="clearfix"></div>
<?php include_once('footer.php'); ?>