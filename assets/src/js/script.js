+jQuery(function($){
	// div background image from data-img attribute
	function sectionBgImg (param){
		this.selector = param.selector,
		this.init = function(){
			var _this = this;
			jQuery(_this.selector).each(function(){
				var yoimg = jQuery(this).attr('data-img');
				if(yoimg != ""){
					jQuery(this).css({
						'background-image' : 'url(' + yoimg + ')'
					});
				}
			});
		}
	}

	// ScrollTop 
	function scrollToTop(param) {
		this.markup = null,
		this.selector = null;
		this.fixed = true;
		this.visible = false;

		this.init = function () {
			if (this.valid()) {
				this.fixed = ( typeof param != 'undefined' && typeof param.fixed != 'undefined' ) ? param.fixed : 'fixed';
				this.selector = ( param && param.selector ) ? param.selector : '#scrollToTop';
				this.getMarkup();
				var _this = this;
				jQuery('body').append(this.markup);
				if (this.fixed) {
					jQuery(this.selector).hide();
					var windowHeight = jQuery(window).height();
					jQuery(window).scroll(function () {
						var scrollPos = jQuery(window).scrollTop();
						if (( scrollPos > ( windowHeight - 100 ) )) {
							if (false == _this.visible) {
								jQuery(_this.selector).fadeIn();
								_this.visible = true;
							}
						} else {
							if (true == _this.visible) {
								jQuery(_this.selector).fadeOut();
								_this.visible = false;
							}
						}
					});

					this.bindEvent();
				}
			}
		}

		this.bindEvent = function () {
			jQuery(document).on('click', this.selector, function (e) {
				e.preventDefault();
				jQuery("html, body").animate({ scrollTop: 0 }, 800);
			});
		}

		this.getMarkup = function () {
			var position = this.fixed;
			var wrapperStyle = 'style="position: ' + position + ';z-index:999999; bottom: 20px; right: 20px;"';
			var buttonStyle = 'style="cursor:pointer;display: inline-block;padding: 10px 20px;background: #f15151;color: #fff;border-radius: 2px;"';
			var markup = '<div ' + wrapperStyle + ' id="scrollToTop"><span ' + buttonStyle + '>Scroll To Top</span></div>';
			this.markup = ( param && param.markup ) ? param.markup : markup;
		}

		this.valid = function () {
			if (param && param.markup && !param.selector) {
				alert('Please provide selector. eg. { markup: "<div id=\'scroll-top\'></div>", selector: "#scroll-top"}');
				return false;
			}
			return true;
		}
	}

	// full height
	function fullScreen(){
		function calculateandAssign(){
			var wheight = jQuery(window).height();
			jQuery('#jsdHeroCarousel .lSSlideWrapper, #jsdHeroCarousel .lSSlideWrapper ul, #jsdHeroCarousel .lSSlideWrapper ul li').height(wheight);
		}
		calculateandAssign();
		jQuery(window).resize(calculateandAssign);
	}
		
	$(document).ready(function(){
		console.log('Hi Developer :)\n'+Array(24).join("-")+'\nhttp://asimsubedi.com.np\n'+Array(24).join("-"));

		new scrollToTop({
			markup: '<div id="scrollToTop"><span></span></div>',
			selector: '#scrollToTop'
		}).init();

		$('#jsdSlide').lightSlider({
			item:1,
			auto:true,
			loop:true,
			pause: 10000,
			mode: 'fade',
			easing: 'ease',
			pager: false,
			prevHtml: '<div class="arrow-left"></div>',
			nextHtml: '<div class="arrow-right"></div>'
		});

		fullScreen();

		$('#jsdMoveDown').on('click', function(e){
			e.preventDefault();
			target = $(this).attr('data-target');
			$('html, body').animate({
		        scrollTop: $(target).offset().top
		    }, 1000);
		});

		// SERVICE Links SCROLL
		$('a[data-target]').on('click', function(e){
			e.preventDefault();
			target = $(this).attr('data-target');
			$('html, body').animate({
		        scrollTop: $(target).offset().top
		    }, 1000);
		});

		$('#jsdCustomerSaying').lightSlider({
			item: 1,
			loop:true,
			slideMargin: 30,
			auto:false,
			pause: 9000,
			speed: 1800,
			mode: 'fade',
			pager: true,
			controls: false,
			pauseOnHover: true,
			easing: 'ease',
			adaptiveHeight: true
		});

		$('#jsdCertificateList').lightSlider({
			item: 5,
			pager: false,
			adaptiveHeight: true,
			prevHtml: '<i class="fa fa-2x fa-angle-left"></i>',
			nextHtml: '<i class="fa fa-2x fa-angle-right"></i>',
			responsive : [
			    {
			        breakpoint:767,
			        settings: {
			            item:3,
			            slideMove:1
			        }
			    }
			]
		});

		$('[id^="serviceGallery"]').lightSlider({
			item: 1,
			pager: false,
			loop: true,
			adaptiveHeight: false,
			prevHtml: '<i class="fa fa-2x fa-angle-left"></i>',
			nextHtml: '<i class="fa fa-2x fa-angle-right"></i>'
		});

		new sectionBgImg({
			'selector' : '[data-img]'
		}).init();

		$('#toggleMenu').on('click', function(e){
			e.preventDefault();
			$(this).toggleClass('open');
			$('.right-navs').toggleClass('navs-open');
		});

		// SERVICE SIDEBAR FIX ON SCROLL
		var sideBarFix = function(param){
			var sideBarPosition = $(param).offset().top,
			sideBarHeight = $(param).height(),
			footerPos = $('.site-footer').offset().top;
			wid = $(param).width();
			$(window).scroll(function(){
				var winPos = $(window).scrollTop();
				if(((sideBarPosition - winPos) < 1) && (footerPos - winPos) > 670){
					$(param).addClass('fixit').width(wid);
				}
				else{
					$(param).removeClass('fixit').width('100%');
				}
			});
		}

		sideBarFix('.jsd-sidebar-menu');
	});
});

function initMap() {
    // Styles a map in night mode.
    var exactLocation = {lat: 29.5643691 , lng: -95.4083207};
    var mapOptions = {
	    center: exactLocation, //lat: 50.334057, , lng: -115.862994
	    zoom: 10,
	    disableDefaultUI: false,
	    zoomControl: false,
	    scrollwheel: false,
	    styles: 
		[
			  {
			    "elementType": "geometry",
			    "stylers": [
			      {
			        "color": "#212121"
			      }
			    ]
			  },
			  {
			    "elementType": "labels.icon",
			    "stylers": [
			      {
			        "visibility": "off"
			      }
			    ]
			  },
			  {
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#757575"
			      }
			    ]
			  },
			  {
			    "elementType": "labels.text.stroke",
			    "stylers": [
			      {
			        "color": "#212121"
			      }
			    ]
			  },
			  {
			    "featureType": "administrative",
			    "elementType": "geometry",
			    "stylers": [
			      {
			        "color": "#757575"
			      }
			    ]
			  },
			  {
			    "featureType": "administrative.country",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#9e9e9e"
			      }
			    ]
			  },
			  {
			    "featureType": "administrative.land_parcel",
			    "stylers": [
			      {
			        "visibility": "off"
			      }
			    ]
			  },
			  {
			    "featureType": "administrative.locality",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#bdbdbd"
			      }
			    ]
			  },
			  {
			    "featureType": "poi",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#757575"
			      }
			    ]
			  },
			  {
			    "featureType": "poi.park",
			    "elementType": "geometry",
			    "stylers": [
			      {
			        "color": "#181818"
			      }
			    ]
			  },
			  {
			    "featureType": "poi.park",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#616161"
			      }
			    ]
			  },
			  {
			    "featureType": "poi.park",
			    "elementType": "labels.text.stroke",
			    "stylers": [
			      {
			        "color": "#1b1b1b"
			      }
			    ]
			  },
			  {
			    "featureType": "road",
			    "elementType": "geometry.fill",
			    "stylers": [
			      {
			        "color": "#2c2c2c"
			      }
			    ]
			  },
			  {
			    "featureType": "road",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#8a8a8a"
			      }
			    ]
			  },
			  {
			    "featureType": "road.arterial",
			    "elementType": "geometry",
			    "stylers": [
			      {
			        "color": "#373737"
			      }
			    ]
			  },
			  {
			    "featureType": "road.highway",
			    "elementType": "geometry",
			    "stylers": [
			      {
			        "color": "#3c3c3c"
			      }
			    ]
			  },
			  {
			    "featureType": "road.highway.controlled_access",
			    "elementType": "geometry",
			    "stylers": [
			      {
			        "color": "#4e4e4e"
			      }
			    ]
			  },
			  {
			    "featureType": "road.local",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#616161"
			      }
			    ]
			  },
			  {
			    "featureType": "transit",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#757575"
			      }
			    ]
			  },
			  {
			    "featureType": "water",
			    "elementType": "geometry",
			    "stylers": [
			      {
			        "color": "#000000"
			      }
			    ]
			  },
			  {
			    "featureType": "water",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      {
			        "color": "#3d3d3d"
			      }
			    ]
			  }
		]
	};
    var map = new google.maps.Map(document.getElementById('jsdMap'), mapOptions);

    var image = 'assets/build/img/map-marker.png';
    var placeMarker = new google.maps.Marker({
      position: exactLocation,
      map: map,
      icon: image
    });

    if(document.getElementById('jsdContactMap')){
	    var contactMap = new google.maps.Map(document.getElementById('jsdContactMap'), mapOptions);
    	var placeMarker2 = new google.maps.Marker({
    	  position: exactLocation,
    	  map: contactMap,
    	  icon: image
    	});
    }
}